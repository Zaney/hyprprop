# hyprprop
xprop for Hyprland

This is a backup of the original Github repo. It does NOT follow or mirror the original. I will do my best to keep this up-to-date. However, I make no promises as this is mainly just for me.

## Dependencies
- `hyprland` obviously
- [hyprevents](https://github.com/vilari-mickopf/hyprevents) capture window changed events to rerun slurp
- `socat` to connect hyprevents with hyprprop events
- `slurp` to select an area on the screen
- `jq` to parse hyprctl output

## Install
```bash
yay -S hyprprop-git
```
or
```bash
sudo make install
```

## Usage
Just run
```bash
hyprprop
```
and select desired window
